class ApplicationController < ActionController::Base

	before_action :current_year

	def current_year
        @current_year = Date.today.year
    end
end
