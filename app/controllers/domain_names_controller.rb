class DomainNamesController < ApplicationController

    # Le nom de domaine a été bien enrégistré et est en attente d'ativation par notre équipe technique, veuillez suivre la procédure de paiement ci-dessous afin que votre domaine soit active !
    
    # Les noms de domaine en .BJ correspondent à l'extension nationale (« ccTLD », Country Code Top-Level Domain) du Bénin
    # Le registre du .BJ est Benin Telecoms.
    
    def recapitulatif
        @domain_name = DomainName.find(params[:id])
        @pic = @domain_name.owner.id_card
    end
    
    def new
        if params[:second_level_domain_name]
            session[:second_level_domain_name] = params[:second_level_domain_name]
        end
        # @second_level_domain_choosed = params[:second_level_domain_name]
        @domain_name = DomainName.new
        @domain_name.build_owner
    end
    
    def create
        @domain_name = DomainName.new(domain_name_params)
        @domain_name.owner.id_card.attach(params[:domain_name][:owner_attributes][:id_card])
        if @domain_name.save
            flash[:notice] = "Votre demande d'enrégistrement du domaine #{@domain_name.domain} a été bien soumit."
            redirect_to recapitulatif_domain_name_path(@domain_name)
        else
            flash.now[:alert] = "Des erreurs ont été trouvées lors de la soumission du formulaire."
            render :new
        end
    end
    
    private
    
    def domain_name_params
        params.require(:domain_name).permit(:domain, :duration, owner_attributes: [:id_card, :first_name, :last_name, :mobile_phone_number, :email_address, :owner_activity_description, :address])
    end
    
end