class Owner < ApplicationRecord
    has_one :domain_name

    validates :first_name, presence: true
    
    has_one_attached :id_card
end
