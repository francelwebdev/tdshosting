class DomainName < ApplicationRecord
    belongs_to :owner
    
    accepts_nested_attributes_for :owner
    
    before_create :set_domain_name_duration_price
    
    private
    
    def set_domain_name_duration_price
        default_pricing = 12500
        
        if self.duration == "1 an"
            self.price = default_pricing
        elsif self.duration == "2 ans"
            self.price = default_pricing * 2
        elsif self.duration == "3 ans"
            self.price = default_pricing * 3
        elsif self.duration == "4 ans"
            self.price = default_pricing * 4
        elsif self.duration == "5 ans"
            self.price = default_pricing * 5
        end    
    end
end
