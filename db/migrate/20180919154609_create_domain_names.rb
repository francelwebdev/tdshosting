class CreateDomainNames < ActiveRecord::Migration[5.2]
  def change
    create_table :domain_names do |t|
      t.string :domain
      t.references :owner, foreign_key: true

      t.timestamps
    end
  end
end
