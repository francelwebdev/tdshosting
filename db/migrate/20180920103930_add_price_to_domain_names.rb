class AddPriceToDomainNames < ActiveRecord::Migration[5.2]
  def change
    add_column :domain_names, :price, :decimal
  end
end
