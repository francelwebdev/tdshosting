class CreateOwners < ActiveRecord::Migration[5.2]
  def change
    create_table :owners do |t|
      t.string :first_name
      t.string :last_name
      t.string :email_address
      t.integer :mobile_phone_number
      t.text :owner_activity_description
      t.string :city
      t.string :address

      t.timestamps
    end
  end
end
