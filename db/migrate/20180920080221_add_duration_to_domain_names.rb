class AddDurationToDomainNames < ActiveRecord::Migration[5.2]
  def change
    add_column :domain_names, :duration, :string
  end
end
